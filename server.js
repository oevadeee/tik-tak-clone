const express = require("express");
const mongoose = require("mongoose");

const Data = require("./data.js");
const Videos = require("./dbModel.js");

// user pass: 2aGKgNcAU1WUupfu

//* app confing
const app = express();
const port = 9000;

//* middlewares
app.use(express.json());

//* DB config
const conection_url =
  "mongodb+srv://admin:2aGKgNcAU1WUupfu@cluster0.7ltyv.mongodb.net/tik-tok?retryWrites=true&w=majority";

mongoose.connect(conection_url, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

//* api endpoints
app.get("/", (req, res) => res.status(200).send("hello wrld"));

app.get("/v1/posts", (req, res) => res.status(200).send(Data));

app.post("/v2/posts", (req, res) => {
  // POST request is to ADD DATA to the database
  // It will let us ADD a video DOCUMENT to the videos COLLECTION
  const dbVideos = req.body;

  Videos.create(dbVideos, (err, data) => {
    if (err) {
      res.status(500).send(err)
    } else {
      res.status(201).send(data);
    };
  });
});

//* listening
app.listen(port, () => console.log(`slucham Cie na ${port}`));
