const mongoose = require('mongoose');

const tiktokSchema = mongoose.Schema({
  url: String,
  chanel: String,
  description: String,
  song: String,
  likes: String,
  messages: String,
  shares: String,
});

//* Collection inside the database
module.exports = mongoose.model('tiktokVideos', tiktokSchema);